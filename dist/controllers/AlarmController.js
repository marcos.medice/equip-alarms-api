"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AlarmController = void 0;
const alarm_1 = require("../models/alarm");
class AlarmController {
    post(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                // verificar se o alarme ja existe
                const query = { $and: [
                        { description: req.body.description },
                        { classification: req.body.classification },
                        { _equipmentId: req.body._equipmentId },
                    ] };
                const isRegisterValid = yield alarm_1.Alarm.find(query);
                if (isRegisterValid && isRegisterValid.length > 0) {
                    res.status(500).json({ status: "error", code: "invalidRegister", message: "Alarme já existente" });
                    return;
                }
                alarm_1.Alarm.create(req.body);
                res.status(200).json("ok");
            }
            catch (error) {
                res.status(500).json('Erro ao cadastrar alarme');
            }
        });
    }
    get(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const query = {};
                // busca por data
                if (req.query.creationDate) {
                    const startDate = new Date(req.query.creationDate.toString());
                    query["creationDate"] = {
                        $gte: startDate,
                        $lt: new Date(startDate.getTime() + (60 * 60 * 24 * 1000))
                    };
                }
                // buscar por descrição
                if (req.query.description) {
                    query["description"] = req.query.description;
                }
                // buscar por classificação (risco)
                if (req.query.classification) {
                    query["classification"] = req.query.classification;
                }
                if (req.query._equipmentId) {
                    query["_equipmentId"] = req.query._equipmentId;
                }
                // buscar
                const alarm = yield alarm_1.Alarm.find(query).sort("-date").populate([{ "path": "equipment" }]);
                res.json({ data: alarm });
            }
            catch (error) {
                res.status(500).json("Alarmes não encontrados");
            }
        });
    }
    put(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            // alterar alarme
            try {
                const alarm = yield alarm_1.Alarm.findOne({ "_id": req.body._id });
                alarm.description = req.body.description;
                alarm.classification = req.body.classification;
                alarm.lastUpdate = new Date();
                // verificar se a alteração já existe
                const query = { $and: [
                        { description: alarm.description },
                        { classification: alarm.classification },
                        { _equipmentId: alarm._equipmentId },
                    ] };
                const isRegisterValid = yield alarm_1.Alarm.find(query);
                if (isRegisterValid && isRegisterValid.length > 0) {
                    console.log("Alarme já existente");
                    res.status(500).json({ status: "error", code: "invalidRegister", message: "Alarme já existente" });
                    return;
                }
                // salvar alteração
                yield alarm.save();
                res.status(200).json("ok");
            }
            catch (error) {
                res.status(500).json('Erro ao atualizar alarme');
            }
        });
    }
    delete(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            // deletar alarme
            try {
                const alarm = yield alarm_1.Alarm.findOne({ "_id": req.params._id });
                alarm.deleteOne();
                res.status(200).json("ok");
            }
            catch (error) {
                res.status(500).json('Erro ao deletar alarme');
            }
        });
    }
}
exports.AlarmController = AlarmController;
//# sourceMappingURL=AlarmController.js.map