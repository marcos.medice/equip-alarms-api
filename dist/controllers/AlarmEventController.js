"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AlarmEventController = void 0;
const alarmEvent_1 = require("../models/alarmEvent");
class AlarmEventController {
    post(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                // verificar se o evento já existe
                const query = { $and: [
                        { _alarmId: req.body._alarmId },
                        { status: true }
                    ] };
                const isRegisterValid = yield alarmEvent_1.AlarmEvent.find(query);
                if (isRegisterValid && isRegisterValid.length > 0) {
                    res.status(500).json({ status: "error", code: "invalidRegister", message: "Evento já ativo" });
                    return;
                }
                // ativação do evento
                alarmEvent_1.AlarmEvent.create(req.body);
                res.status(200).json("ok");
            }
            catch (error) {
                res.status(500).json('Erro ao cadastrar evento');
            }
        });
    }
    get(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const query = {};
                // busca pela data do evento
                if (req.query.activationDate) {
                    const startDate = new Date(req.query.date.toString());
                    query["activationDate"] = {
                        $gte: startDate,
                        $lt: new Date(startDate.getTime() + (60 * 60 * 24 * 1000))
                    };
                }
                if (req.query.status) {
                    query["status"] = req.query.status;
                }
                // busca através do alarme
                if (req.query._alarmId) {
                    query["_alarmId"] = req.query._alarmId;
                }
                // busca
                const events = yield alarmEvent_1.AlarmEvent.find(query).sort("-date").populate([{ "path": "alarm", populate: { "path": "equipment" } }]);
                res.json({ data: events });
            }
            catch (error) {
                res.status(500).json("Eventos não encontrados");
            }
        });
    }
    put(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            // desativar evento
            try {
                const event = yield alarmEvent_1.AlarmEvent.findOne({ "_id": req.body._id });
                if (event.status) {
                    event.status = false;
                    event.deactivationDate = new Date();
                    yield event.save();
                    res.status(200).json("ok");
                }
                else {
                    res.status(500).json('Alarme já desativado');
                }
            }
            catch (error) {
                res.status(500).json('Erro ao atualizar evento');
            }
        });
    }
}
exports.AlarmEventController = AlarmEventController;
//# sourceMappingURL=AlarmEventController.js.map