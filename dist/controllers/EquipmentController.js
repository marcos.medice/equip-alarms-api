"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EquipmentController = void 0;
const equipment_1 = require("../models/equipment");
class EquipmentController {
    post(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                // verificar se o equipamento ja existe
                const query = { serialNumber: req.body.serialNumber };
                const isRegisterValid = yield equipment_1.Equipment.find(query);
                if (isRegisterValid && isRegisterValid.length > 0) {
                    res.status(500).json({ status: "error", code: "invalidRegister", message: "Equipamento já existente" });
                    return;
                }
                // registrar equipamento
                equipment_1.Equipment.create(req.body);
                res.status(200).json("ok");
            }
            catch (error) {
                res.status(500).json('Erro ao cadastrar equipamento');
            }
        });
    }
    get(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const query = {};
                // busca pela data
                if (req.query.creationDate) {
                    const startDate = new Date(req.query.creationDate.toString());
                    query["creationDate"] = {
                        $gte: startDate,
                        $lt: new Date(startDate.getTime() + (60 * 60 * 24 * 1000))
                    };
                }
                // busca pelo nome
                if (req.query.name) {
                    query["name"] = req.query.name;
                }
                // busca pelo serial number
                if (req.query.serialNumber) {
                    query["serialNumber"] = req.query.serialNumber;
                }
                // busca pelo tipo de equipamento
                if (req.query.equipType) {
                    query["equipType"] = req.query.equipType;
                }
                // busca
                const equipments = yield equipment_1.Equipment.find(query).sort("-date");
                res.json({ data: equipments });
            }
            catch (error) {
                res.status(500).json("Equipamentos não encontrados");
            }
        });
    }
    put(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            // alterar equipamento
            try {
                const equipment = yield equipment_1.Equipment.findOne({ "_id": req.body._id });
                equipment.name = req.body.name;
                equipment.serialNumber = req.body.serialNumber;
                equipment.equipType = req.body.equipType;
                equipment.lastUpdate = new Date();
                // verificar se o equipamento ja existe
                const query = { serialNumber: equipment.serialNumber };
                const isRegisterValid = yield equipment_1.Equipment.find(query);
                if (isRegisterValid && isRegisterValid.length > 0) {
                    res.status(500).json({ status: "error", code: "invalidRegister", message: "Equipamento já existente" });
                    return;
                }
                // atualizar equipamento
                yield equipment.save();
                res.status(200).json("ok");
            }
            catch (error) {
                res.status(500).json('Erro ao atualizar equipamento');
            }
        });
    }
    delete(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            // deletar equipamento
            try {
                const equipment = yield equipment_1.Equipment.findOne({ "_id": req.params._id });
                equipment.deleteOne();
                res.status(200).json("ok");
            }
            catch (error) {
                res.status(500).json('Erro ao deletar equipamento');
            }
        });
    }
}
exports.EquipmentController = EquipmentController;
//# sourceMappingURL=EquipmentController.js.map