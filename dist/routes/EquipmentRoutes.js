"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EquipmentRoutes = void 0;
const express_1 = require("express");
const EquipmentController_1 = require("../controllers/EquipmentController");
class EquipmentRoutes {
    constructor() {
        this.equipmentController = new EquipmentController_1.EquipmentController();
        this.routes();
    }
    routes() {
        this.router = express_1.Router();
        this.router.post("", this.equipmentController.post);
        this.router.get("", this.equipmentController.get);
        this.router.put("", this.equipmentController.put);
        this.router.delete("/:_id", this.equipmentController.delete);
    }
}
exports.EquipmentRoutes = EquipmentRoutes;
//# sourceMappingURL=EquipmentRoutes.js.map