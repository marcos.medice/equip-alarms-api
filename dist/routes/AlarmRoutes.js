"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AlarmRoutes = void 0;
const express_1 = require("express");
const AlarmController_1 = require("../controllers/AlarmController");
class AlarmRoutes {
    constructor() {
        this.alarmController = new AlarmController_1.AlarmController();
        this.routes();
    }
    routes() {
        this.router = express_1.Router();
        this.router.post("", this.alarmController.post);
        this.router.get("", this.alarmController.get);
        this.router.put("", this.alarmController.put);
        this.router.delete("/:_id", this.alarmController.delete);
    }
}
exports.AlarmRoutes = AlarmRoutes;
//# sourceMappingURL=AlarmRoutes.js.map