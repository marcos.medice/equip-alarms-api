"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AlarmEventRoutes = void 0;
const express_1 = require("express");
const AlarmEventController_1 = require("../controllers/AlarmEventController");
class AlarmEventRoutes {
    constructor() {
        this.alarmEventController = new AlarmEventController_1.AlarmEventController();
        this.routes();
    }
    routes() {
        this.router = express_1.Router();
        this.router.post("", this.alarmEventController.post);
        this.router.get("", this.alarmEventController.get);
        this.router.get("/:status", this.alarmEventController.get);
        this.router.put("", this.alarmEventController.put);
    }
}
exports.AlarmEventRoutes = AlarmEventRoutes;
//# sourceMappingURL=AlarmEventRoutes.js.map