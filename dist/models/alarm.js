"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Alarm = exports.alarmSchema = void 0;
const mongoose_1 = require("mongoose");
exports.alarmSchema = new mongoose_1.Schema({
    description: String,
    classification: String,
    _equipmentId: {
        type: mongoose_1.Schema.Types.ObjectId, required: true, ref: "Equipment"
    },
    creationDate: {
        type: Date,
        default: Date.now
    },
    lastUpdate: {
        type: Date,
        default: Date.now
    }
});
let options;
options = {};
options.virtuals = true;
exports.alarmSchema.set("toJSON", options);
exports.alarmSchema.virtual("equipment", {
    ref: "Equipment",
    localField: "_equipmentId",
    foreignField: "_id",
    justOne: true
});
exports.Alarm = mongoose_1.model("Alarm", exports.alarmSchema, "alarm");
//# sourceMappingURL=alarm.js.map