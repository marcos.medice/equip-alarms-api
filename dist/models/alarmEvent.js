"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AlarmEvent = exports.alarmEventSchema = void 0;
const mongoose_1 = require("mongoose");
exports.alarmEventSchema = new mongoose_1.Schema({
    _alarmId: {
        type: mongoose_1.Schema.Types.ObjectId, required: true, ref: "Alarm"
    },
    // dados do evento
    activationDate: {
        type: Date,
        default: Date.now
    },
    deactivationDate: {
        type: Date,
        default: null
    },
    status: {
        type: Boolean,
        default: true
    }
});
let options;
options = {};
options.virtuals = true;
exports.alarmEventSchema.set("toJSON", options);
exports.alarmEventSchema.virtual("alarm", {
    ref: "Alarm",
    localField: "_alarmId",
    foreignField: "_id",
    justOne: true
});
exports.AlarmEvent = mongoose_1.model("AlarmEvent", exports.alarmEventSchema, "alarmEvent");
//# sourceMappingURL=alarmEvent.js.map