"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Equipment = exports.equipmentSchema = void 0;
const mongoose_1 = require("mongoose");
exports.equipmentSchema = new mongoose_1.Schema({
    name: String,
    serialNumber: String,
    equipType: {
        tensao: String,
        corrente: String,
        oleo: String
    },
    creationDate: {
        type: Date,
        default: Date.now
    },
    lastUpdate: {
        type: Date,
        default: Date.now
    }
});
let options;
options = {};
options.virtuals = true;
exports.equipmentSchema.set("toJSON", options);
exports.Equipment = mongoose_1.model("Equipment", exports.equipmentSchema, "equipment");
//# sourceMappingURL=equipment.js.map