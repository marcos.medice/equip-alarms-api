"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv/config");
const mongo_1 = __importDefault(require("./config/mongo"));
const express_1 = __importDefault(require("express"));
const compression_1 = __importDefault(require("compression"));
const cors_1 = __importDefault(require("cors"));
const mongoose_1 = __importDefault(require("mongoose"));
const EquipmentRoutes_1 = require("./routes/EquipmentRoutes");
const AlarmEventRoutes_1 = require("./routes/AlarmEventRoutes");
const AlarmRoutes_1 = require("./routes/AlarmRoutes");
const morgan = require("morgan");
class Server {
    constructor() {
        this.app = express_1.default();
        this.config();
        this.routes();
        this.mongo();
    }
    routes() {
        this.app.use("/api/v1/alarms", new AlarmRoutes_1.AlarmRoutes().router);
        this.app.use("/api/v1/equipments", new EquipmentRoutes_1.EquipmentRoutes().router);
        this.app.use("/api/v1/events", new AlarmEventRoutes_1.AlarmEventRoutes().router);
    }
    config() {
        this.app.set("port", process.env.PORT || 3000);
        this.app.use(morgan(":remote-addr :remote-user :method :url HTTP/:http-version :status :res[content-length] - :response-time ms"));
        this.app.use(express_1.default.json());
        this.app.use(express_1.default.urlencoded({ extended: false }));
        this.app.use(compression_1.default());
        this.app.use(cors_1.default());
    }
    mongo() {
        const connection = mongoose_1.default.connection;
        mongoose_1.default.set("useFindAndModify", false);
        connection.on("connected", () => {
            console.log("Mongo Connection Established");
        });
        connection.on("reconnected", () => {
            console.log("Mongo Connection Reestablished");
        });
        connection.on("disconnected", () => {
            console.log("Mongo Connection Disconnected");
        });
        connection.on("close", () => {
            console.log("Mongo Connection Closed");
        });
        connection.on("error", (error) => {
            console.log("Mongo Connection ERROR: " + error);
        });
        const run = () => __awaiter(this, void 0, void 0, function* () {
            yield mongoose_1.default.connect(mongo_1.default.connection, {
                autoReconnect: true, keepAlive: true, connectTimeoutMS: 10000, maxPoolSize: 10, autoIndex: true
            });
        });
        run().catch(error => console.error(error));
    }
    start() {
        this.app.listen(this.app.get("port"), () => {
            //   if (_error) {
            //     return console.log("Error: ", _error);
            //   }
            return console.log("\x1b[33m%s\x1b[0m", "Server :: Running @ 'http://localhost:'" + this.app.get("port"));
        });
    }
}
const server = new Server();
server.start();
//# sourceMappingURL=server.js.map