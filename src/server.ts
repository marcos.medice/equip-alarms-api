
import "dotenv/config";

import mongoConfig from "./config/mongo";

import express from "express";
import compression from "compression";
import cors from "cors";
import mongoose from "mongoose";
import { EquipmentRoutes } from "./routes/EquipmentRoutes";
import { AlarmEventRoutes } from "./routes/AlarmEventRoutes";
import { AlarmRoutes } from "./routes/AlarmRoutes";

const morgan = require("morgan");

class Server {
  public app: express.Application;

  constructor() {
    this.app = express();
    this.config();
    this.routes();
    this.mongo();
  }

  public routes(): void {
    this.app.use("/api/v1/alarms", new AlarmRoutes().router);
    this.app.use("/api/v1/equipments", new EquipmentRoutes().router);
    this.app.use("/api/v1/events", new AlarmEventRoutes().router);
  }

  public config(): void {
    this.app.set("port", process.env.PORT || 3000);
    this.app.use(morgan(":remote-addr :remote-user :method :url HTTP/:http-version :status :res[content-length] - :response-time ms"));
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: false }));
    this.app.use(compression());
    this.app.use(cors());
  }

  private mongo() {
    const connection = mongoose.connection;

    mongoose.set("useFindAndModify", false);
    connection.on("connected", () => {
      console.log("Mongo Connection Established");
    });
    connection.on("reconnected", () => {
      console.log("Mongo Connection Reestablished");
    });
    connection.on("disconnected", () => {
      console.log("Mongo Connection Disconnected");
    });
    connection.on("close", () => {
      console.log("Mongo Connection Closed");
    });
    connection.on("error", (error: Error) => {
      console.log("Mongo Connection ERROR: " + error);
    });

    const run = async () => {
      await mongoose.connect(mongoConfig.connection, {
        autoReconnect: true, keepAlive: true, connectTimeoutMS: 10000, maxPoolSize: 10, autoIndex: true
      });
    };
    run().catch(error => console.error(error));


  }


  public start(): void {
    this.app.listen(this.app.get("port"), () => {
    //   if (_error) {
    //     return console.log("Error: ", _error);
    //   }

      return console.log("\x1b[33m%s\x1b[0m", "Server :: Running @ 'http://localhost:'" + this.app.get("port"));
    });
  }

}

const server = new Server();

server.start();
