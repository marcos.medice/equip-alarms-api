import { Request, Response } from "express";
import { AlarmEvent } from "../models/alarmEvent";
import { Alarm } from "../models/alarm";
import { Equipment } from "../models/equipment";

export class AlarmEventController {

    public async post(req: Request, res: Response): Promise<void> {

        try {

          // verificar se o evento já existe
          const query = {$and: [
            { _alarmId: req.body._alarmId },
            { status: true }
          ]};

          const isRegisterValid = await AlarmEvent.find(query);

          if (isRegisterValid && isRegisterValid.length > 0) {
            res.status(500).json({ status: "error", code: "invalidRegister", message: "Evento já ativo" });
            return;
          }

          // ativação do evento
          AlarmEvent.create(req.body);
          res.status(200).json("ok");          

        } catch (error) {
            res.status(500).json('Erro ao cadastrar evento');
        }

    }

    public async get(req: Request, res: Response): Promise<void> {
        try {

          const query: any = {};
          
          // busca pela data do evento
          if (req.query.activationDate) {
            const startDate = new Date(req.query.date.toString());
            query["activationDate"] = {
              $gte: startDate,
              $lt: new Date(startDate.getTime() + (60 * 60 * 24 * 1000))
            };
          }
    
          if (req.query.status) {
            query["status"] = req.query.status;
          }

          // busca através do alarme
          if (req.query._alarmId) {
            query["_alarmId"] = req.query._alarmId;
          }

          // busca
          const events = await AlarmEvent.find(query).sort("-date").populate([{ "path": "alarm", populate : { "path" : "equipment"}}]);
          res.json({ data: events });
    
        } catch (error) {
          res.status(500).json("Eventos não encontrados");
        }
    }

    public async put(req: Request, res: Response): Promise<void> {

      // desativar evento
      try {

        const event = await AlarmEvent.findOne({ "_id": req.body._id });

        if(event.status) {
            event.status = false;
            event.deactivationDate = new Date();

            await event.save();
            res.status(200).json("ok");
        } else {
            res.status(500).json('Alarme já desativado');
        }

      } catch (error) {
          res.status(500).json('Erro ao atualizar evento');
      }

  }

    // public async delete(req: Request, res: Response): Promise<void> {
      
    //   try {
    //       const event = await AlarmEvent.findOne({ "_id": req.body._id });

    //       event.deleteOne();
    //       res.status(200).json("ok");

    //   } catch (error) {
    //       res.status(500).json('Erro ao deletar evento');
    //   }
    // }
}