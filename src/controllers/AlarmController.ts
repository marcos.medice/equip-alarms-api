import { Request, Response } from "express";
import { Alarm } from "../models/alarm";
import { Equipment } from "../models/equipment";

export class AlarmController {

    public async post(req: Request, res: Response): Promise<void> {

      try {
        // verificar se o alarme ja existe
        const query = {$and: [
          {description: req.body.description},
          {classification: req.body.classification},
          {_equipmentId: req.body._equipmentId},
        ]};
  
        const isRegisterValid = await Alarm.find(query);
  
        if (isRegisterValid && isRegisterValid.length > 0) {
          res.status(500).json({ status: "error", code: "invalidRegister", message: "Alarme já existente" });
          return;
        }

        Alarm.create(req.body);
        res.status(200).json("ok");

      } catch (error) {
        res.status(500).json('Erro ao cadastrar alarme');
      }
    }

    public async get(req: Request, res: Response): Promise<void> {
      try {
    
        const query: any = {};
    
        // busca por data
        if (req.query.creationDate) {
          const startDate = new Date(req.query.creationDate.toString());
          query["creationDate"] = {
            $gte: startDate,
            $lt: new Date(startDate.getTime() + (60 * 60 * 24 * 1000))
          };
        }
  
        // buscar por descrição
        if (req.query.description) {
          query["description"] = req.query.description;
        }

        // buscar por classificação (risco)
        if (req.query.classification) {
          query["classification"] = req.query.classification;
        }

        if (req.query._equipmentId) {
          query["_equipmentId"] = req.query._equipmentId;
        }

        // buscar
        const alarm = await Alarm.find(query).sort("-date").populate([{ "path": "equipment" }]);
        res.json({ data: alarm });
  
      } catch (error) {
        res.status(500).json("Alarmes não encontrados");
      }
    }

    public async put(req: Request, res: Response): Promise<void> {

      // alterar alarme
      try {
        const alarm = await Alarm.findOne({ "_id": req.body._id });
        alarm.description = req.body.description;
        alarm.classification = req.body.classification;
        alarm.lastUpdate = new Date();

        // verificar se a alteração já existe
        const query = {$and: [
          {description: alarm.description},
          {classification: alarm.classification},
          {_equipmentId: alarm._equipmentId},
        ]};
  
        const isRegisterValid = await Alarm.find(query);
  
        if (isRegisterValid && isRegisterValid.length > 0) {
          console.log("Alarme já existente");
          res.status(500).json({ status: "error", code: "invalidRegister", message: "Alarme já existente" });
          return;
        }

        // salvar alteração
        await alarm.save();
        res.status(200).json("ok");

      } catch (error) {
        res.status(500).json('Erro ao atualizar alarme');
      }
    }

    public async delete(req: Request, res: Response): Promise<void> {

      // deletar alarme
      try {
        const alarm = await Alarm.findOne({ "_id": req.params._id });

        alarm.deleteOne();
        res.status(200).json("ok");

      } catch (error) {
          res.status(500).json('Erro ao deletar alarme');
      }
    }
}