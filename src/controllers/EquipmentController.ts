import { Request, Response } from "express";
import { Equipment } from "../models/equipment";

export class EquipmentController {

    public async post(req: Request, res: Response): Promise<void> {

      try {

        // verificar se o equipamento ja existe
        const query = { serialNumber: req.body.serialNumber };
  
        const isRegisterValid = await Equipment.find(query);
  
        if (isRegisterValid && isRegisterValid.length > 0) {
          res.status(500).json({ status: "error", code: "invalidRegister", message: "Equipamento já existente" });
          return;
        }
        
        // registrar equipamento
        Equipment.create(req.body);
        res.status(200).json("ok");

      } catch (error) {
        res.status(500).json('Erro ao cadastrar equipamento');
      }

    }

    public async get(req: Request, res: Response): Promise<void> {
        
      try {
          
        const query: any = {};
  
        // busca pela data
        if (req.query.creationDate) {
          const startDate = new Date(req.query.creationDate.toString());
          query["creationDate"] = {
            $gte: startDate,
            $lt: new Date(startDate.getTime() + (60 * 60 * 24 * 1000))
          };
        }

        // busca pelo nome
        if (req.query.name) {
          query["name"] = req.query.name;
        }

        // busca pelo serial number
        if (req.query.serialNumber) {
          query["serialNumber"] = req.query.serialNumber;
        }
        
        // busca pelo tipo de equipamento
        if (req.query.equipType) {
          query["equipType"] = req.query.equipType;
        }

        // busca
        const equipments = await Equipment.find(query).sort("-date");
        res.json({ data: equipments });
    
      } catch (error) {
        res.status(500).json("Equipamentos não encontrados");
      }
      
    }

    public async put(req: Request, res: Response): Promise<void> {

      // alterar equipamento
      try {
        const equipment = await Equipment.findOne({ "_id": req.body._id });
        equipment.name = req.body.name;
        equipment.serialNumber = req.body.serialNumber;
        equipment.equipType = req.body.equipType;
        equipment.lastUpdate = new Date();

        // verificar se o equipamento ja existe
        const query = { serialNumber: equipment.serialNumber };
  
        const isRegisterValid = await Equipment.find(query);
  
        if (isRegisterValid && isRegisterValid.length > 0) {
          res.status(500).json({ status: "error", code: "invalidRegister", message: "Equipamento já existente" });
          return;
        }

        // atualizar equipamento
        await equipment.save();
        res.status(200).json("ok");

      } catch (error) {
        res.status(500).json('Erro ao atualizar equipamento');
      }
    }

    public async delete(req: Request, res: Response): Promise<void> {

      // deletar equipamento
      try {
        
        const equipment = await Equipment.findOne({ "_id": req.params._id });

        equipment.deleteOne();
        res.status(200).json("ok");

      } catch (error) {
        res.status(500).json('Erro ao deletar equipamento');
      }
    }

}