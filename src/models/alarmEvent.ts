import { Schema, Document, model, Model } from "mongoose";

export interface IAlarmEvent extends Document {
    _alarmId: string;
    activationDate: Date;
    deactivationDate: Date;
    status: boolean;
}

export const alarmEventSchema = new Schema({

    _alarmId: {
        type: Schema.Types.ObjectId, required: true, ref: "Alarm"
    },

    // dados do evento
    activationDate: {
        type: Date,
        default: Date.now
    },
    deactivationDate: {
        type: Date,
        default: null
    },
    status: {
        type: Boolean,
        default: true
    }
});

interface Options {
    virtuals?: boolean;
    hide?: string;
    transform?: any;
}

let options: Options;
options = {};

options.virtuals = true;

alarmEventSchema.set("toJSON", options);

alarmEventSchema.virtual("alarm", {
    ref: "Alarm",
    localField: "_alarmId",
    foreignField: "_id",
    justOne: true
});
  
export const AlarmEvent: Model<IAlarmEvent> = model<IAlarmEvent>("AlarmEvent", alarmEventSchema, "alarmEvent");