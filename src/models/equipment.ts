import { Schema, Document, model, Model } from "mongoose";

export interface IEquipment extends Document {
    name: String;
    serialNumber: String;
    equipType: {
        tensao: String,
        corrente: String,
        oleo: String
    };
    creationDate: Date;
    lastUpdate: Date;
}

export const equipmentSchema = new Schema({
    name: String,
    serialNumber: String,
    equipType: {
        tensao: String,
        corrente: String,
        oleo: String
    }, 
    creationDate: {
        type: Date,
        default: Date.now
    },
    lastUpdate: {
        type: Date,
        default: Date.now
    }
});

interface Options {
    virtuals?: boolean;
    hide?: string;
    transform?: any;
}

let options: Options;
options = {};

options.virtuals = true;

equipmentSchema.set("toJSON", options);

export const Equipment: Model<IEquipment> = model<IEquipment>("Equipment", equipmentSchema, "equipment");