import { Schema, Document, model, Model } from "mongoose";

export interface IAlarm extends Document {
    description: string;
    classification: string;
    _equipmentId: string;
    creationDate: Date;
    lastUpdate: Date;
}

export const alarmSchema = new Schema({
    description: String,
    classification: String,
    _equipmentId: {
        type: Schema.Types.ObjectId, required: true, ref: "Equipment"
    },
    creationDate: {
        type: Date,
        default: Date.now
    },
    lastUpdate: {
        type: Date,
        default: Date.now
    }
});

interface Options {
    virtuals?: boolean;
    hide?: string;
    transform?: any;
}

let options: Options;
options = {};

options.virtuals = true;

alarmSchema.set("toJSON", options);

alarmSchema.virtual("equipment", {
    ref: "Equipment",
    localField: "_equipmentId",
    foreignField: "_id",
    justOne: true
});
  
export const Alarm: Model<IAlarm> = model<IAlarm>("Alarm", alarmSchema, "alarm");