import { Router } from "express";
import { AlarmEventController } from "../controllers/AlarmEventController";

export class AlarmEventRoutes {

    public router: Router;

    public alarmEventController: AlarmEventController = new AlarmEventController();

    constructor(){
        this.routes();
    }

    public routes(): void {
        this.router = Router();
        this.router.post("", this.alarmEventController.post);
        this.router.get("", this.alarmEventController.get);
        this.router.get("/:status", this.alarmEventController.get);
        this.router.put("", this.alarmEventController.put);
    }

}