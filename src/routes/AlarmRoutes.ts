import { Router } from "express";
import { AlarmController } from "../controllers/AlarmController";

export class AlarmRoutes {

    public router: Router;

    public alarmController: AlarmController = new AlarmController();

    constructor(){
        this.routes();
    }

    public routes(): void {
        this.router = Router();
        this.router.post("", this.alarmController.post);
        this.router.get("", this.alarmController.get);
        this.router.put("", this.alarmController.put);
        this.router.delete("/:_id", this.alarmController.delete);
    }

}