import { Router } from "express";
import { EquipmentController } from "../controllers/EquipmentController";

export class EquipmentRoutes {

    public router: Router;

    public equipmentController: EquipmentController = new EquipmentController();

    constructor(){
        this.routes();
    }

    public routes(): void {
        this.router = Router();
        this.router.post("", this.equipmentController.post);
        this.router.get("", this.equipmentController.get);
        this.router.put("", this.equipmentController.put);
        this.router.delete("/:_id", this.equipmentController.delete);
    }

}